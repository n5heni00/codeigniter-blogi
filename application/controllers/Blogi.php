<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Blogi extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Blogi_Model');
        $this->load->helper('url');
        $this->load->helper('form');
    }

    public function index() {

        $config['base_url'] = site_url('blogi/index');
        $data['kirjoitukset'] = $this->Blogi_Model->hae_kaikki();
        $data['navigation'] = 'nav/navigation_view';
        $data['main_content'] = 'kirjoitus/kirjoitukset_view';
        $this->load->view('template', $data);
    }

    public function siirry($id) {
        $data['kirjoitukset'] = $this->Blogi_Model->hae($id);
        $data['kommentit'] = $this->Blogi_Model->hae_kommentit($id);
        $data['navigation'] = 'nav/navigation_view';
        $data['main_content'] = 'kirjoitus/kirjoitus_view';
        $this->load->view('template', $data);
    }

    public function lisaa() {
        $data = array(
            'id' => '',
            'otsikko' => '',
            'paivays' => '',
            'teksti' => ''
        );
        $data['main_content'] = 'kirjoitus/lisaakirjoitus_view';
        $data['navigation'] = 'nav/navigation_view';
        $this->load->view('template', $data);
    }

    public function kommentoi() {
        $data = array(
//            'id' => '',
            'kirjoitus_id' => $this->input->post('kirjoitusId'),
            'teksti' => $this->input->post('kommenttiTeksti'),
            'kayttaja_id' => 1
        );
        $this->Blogi_Model->lisaaKommentti($data);
        redirect('blogi/siirry/' . $this->input->post('kirjoitusId') . '', 'refresh');
    }

    public function poista($id) {
        $this->Blogi_Model->poistaKommentit(intval($id));
        $this->Blogi_Model->poista(intval($id));
        $this->index();
    }

    public function poistaKommentti($id) {
        $this->Blogi_Model->poistaKommentti(intval($id));
        redirect('blogi/index', 'refresh');
    }

    public function tallenna() {
        
        $data = array(
            'otsikko' => $this->input->post('otsikko'),
            'teksti' => $this->input->post('teksti'),
            'kayttaja_id' => 1
        );
        $this->Blogi_Model->lisaa($data);
        redirect('blogi/index', 'refresh');
    }

    public function tallennaKayttaja() {

        $data = array(
            'sukunimi' => $this->input->post('sukunimi'),
            'etunimi' => $this->input->post('etunimi'),
            'tunnus' => $this->input->post('tunnus'),
            'email' => $this->input->post('sahkoposti'),
            'salasana' => $this->input->post('salasana')
        );
        $this->Blogi_Model->lisaaKayttaja($data);
        redirect('blogi/index', 'refresh');
    }

    public function lisaaKayttaja() {
        $data['main_content'] = 'kirjoitus/rekisteroidy_view';
        $data['navigation'] = 'nav/navigation_view';
        $this->load->view('template', $data);
    }
}
