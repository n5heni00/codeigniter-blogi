<?php

class Blogi_Model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function hae_kaikki() {

        $this->db->select();
        $this->db->from('kirjoitus', 'kayttaja');
        $kysely = $this->db->get();
        return $kysely->result();
    }
    
    public function hae_kommentit($id) {
        $this->db->select();
        $this->db->from('kommentti');
        $this->db->where('kirjoitus_id', $id);
        $kysely = $this->db->get();
        return $kysely->result();
    }

    public function hae_kayttaja() {
        $kysely_inp = 'select kayttaja.tunnus as kayttaja_tunnus from kayttaja';
        $kysely = $this->db->query($kysely_inp);
        return $kysely->row()->kayttaja_tunnus;
    }

    public function hae($id) {
        $this->db->where('id', $id);
        $kysely = $this->db->get('kirjoitus');
        return $kysely->row($id);
    }

    public function lisaa($data) {
        $this->db->insert('kirjoitus', $data);
        return $this->db->insert_id();
    }
    
    public function lisaaKayttaja($data) {
        $this->db->insert('kayttaja', $data);
        return $this->db->insert_id();
    }

    public function lisaaKommentti($data) {
        $this->db->insert('kommentti', $data);
        return $this->db->insert_id();
    }

    public function poista($id) {
        $this->db->where('id', $id);
        $this->db->delete('kirjoitus');
    }
    public function poistaKommentti($id) {
        $this->db->where('id', $id);
        $this->db->delete('kommentti');
    }
    public function poistaKommentit($id) {
        $this->db->where('kirjoitus_id', $id);
        $this->db->delete('kommentti');
    }
}
