
<?php
print "<br><h1>" . $kirjoitukset->otsikko . "</h1>";
$date = new DateTime($kirjoitukset -> paivays);
print "<p>";
print $date->format('d.m.Y H.i');
print " by ";
print $this->Blogi_Model->hae_kayttaja();
print "</p>";
print "<p>" . $kirjoitukset->teksti . "</p>";
?>
<form action="<?php echo site_url('blogi/kommentoi') ?>" method="post">
    <br>
    <label>Kommentoi:</label>
    <div class="form-group">
        <input type="hidden" name="kirjoitusId" id="kirjoitusId" value="<?php echo $kirjoitukset->id ?>" />
        <textarea type="text" rows="2" class="form-control" name="kommenttiTeksti"></textarea>
    </div>
    <input type="submit"><br><br>
</form>
<a href = "<?php echo site_url('blogi/index') ?>">Takaisin etusivulle.</a><br><br><br>
<?php
foreach ($kommentit as $kommentti) {
    print "<div>";
    print "<p>";
    print $kommentti->teksti;
    print "<br>";
    $date = new DateTime($kommentti->paivays);
    print $date->format('d.m.Y H.i');
    print " by ";
    print $this->Blogi_Model->hae_kayttaja();
    print "</p>";
    echo "<a href='" . site_url('blogi/poistaKommentti/'
            . $kommentti->id) . "' ><span class='glyphicon glyphicon-trash'>Poista</span></a><hr>";
    print "</div>";
}