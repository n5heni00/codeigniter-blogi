<div class="container">
    <form action="<?php echo site_url('blogi/tallennaKayttaja')?>" method="post">
        <br>
        <h1>Uusi käyttäjä</h1>
        <div class="form-group">
            <label for="sukunimi">Sukunimi</label>
            <input type="text" required="required" class="form-control" name="sukunimi">
        </div>
        <div class="form-group">
            <label for="etunimi">Etunimi</label>
            <input type="text" required="required" class="form-control" name="etunimi">
        </div>
        <div class="form-group">
            <label for="tunnus">Käyttäjätunnus</label>
            <input type="text" required="required" class="form-control" name="tunnus">
        </div>
        <div class="form-group">
            <label for="sahkoposti">Sähköposti</label>
            <input type="email" required="required" class="form-control" name="sahkoposti">
        </div>
        <div class="form-group">
            <label for="sukunimi">Salasana</label>
            <input type="password" required="required" class="form-control" name="salasana">
        </div>
        <input type="submit"><br><br><br>
        <a href="<?php echo site_url('blogi/index')?>">Takaisin etusivulle.</a>
    </form>
</div>